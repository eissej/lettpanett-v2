<footer class="page-footer topfooter">
	<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<div class="clearfix">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('copy area') ) ; ?>
				</div>
			</div>
		</div>
	</div>
	</div>
</footer>

<!-- End Document
================================================== -->
<?php wp_footer(); ?>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/bin/materialize.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/prism.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<!--script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script-->
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
 <!--scrollbar-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
</body>
</html>