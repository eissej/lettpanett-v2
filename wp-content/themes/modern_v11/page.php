<?php get_header(); ?>
<main>
<div class="bodyWrapper" style="position:relative;">
	<div class="wrapper">
		<div class="content-wrapper  container clearfix">
			<div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if(!is_front_page()){?>
					<div class="col s12">
						<div class="postHeader clearfix">
							<h1 class="post_title withbottomBorder"><?php the_title(); ?></h1>
						</div>
					</div>
				<?php } ?>
				<div class="post_ col s12 m9">
					<div class="postContent">
						<div class="postText">
							<?php the_content();?>
						</div>
					</div>
				</div>
				<div class="col s12 m3">
					<div class="sidewidgetsWrapper" >
						<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidewidget') ) ; ?>
					</div>
				</div>
			</div>
			<?php endwhile; // End the loop. Whew. ?>
			</div>
		</div>
	</div>
	<div class="fullwidth bluebg">
		<div class="wrapper">
			<div class="container clearfix">
				<div class="row">
				<div class="col s12">
					<div class="bottomformWrapper">
						<center><h1>Try our customer solution for your business now today!<span>Simply fill in the form below to start...</span></h1></center>
						<?php gravity_form( 1, false, false, false, '', false ); ?>
					</div>
				</div>
				<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('default bottom widget area') ) ; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
<?php get_footer(); ?>