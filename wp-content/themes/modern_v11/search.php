<?php get_header(); ?>
<main>
<div class="bodyWrapper" style="position:relative;">
	<div class="wrapper">
		<div class="content-wrapper  container clearfix">
			<div class="row">
				<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'lettpanett' ), get_search_query() ); ?></h2>
				</header><!-- .page-header -->

				<?php
				// Start the loop.
				while ( have_posts() ) : the_post(); ?>

					<?php
					/*
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'content', 'search' );

				// End the loop.
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination( array(
					'prev_text'          => __( 'Previous page', 'lettpanett' ),
					'next_text'          => __( 'Next page', 'lettpanett' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'lettpanett' ) . ' </span>',
				) );

			// If no content, include the "No posts found" template.
			else :
				get_template_part( 'content', 'none' );

			endif;
			?>
			</div>
		</div>
	</div>
	<div class="fullwidth bluebg">
		<div class="wrapper">
			<div class="container clearfix">
				<div class="row">
				<div class="col s12">
					<div class="bottomformWrapper">
						<center><h1>Try our customer solution for your business now today!<span>Simply fill in the form below to start...</span></h1></center>
						<?php gravity_form( 1, false, false, false, '', false ); ?>
					</div>
				</div>
				<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('default bottom widget area') ) ; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
<?php get_footer(); ?>