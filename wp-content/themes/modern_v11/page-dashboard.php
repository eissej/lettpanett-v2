<?php/*Template Name: Dashboard Template*/?>
<?php get_header('dashboard'); ?>
<main>
<div id="main">
	<!-- LEFT SIDEBAR -->
			<aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation <?php if ( is_user_logged_in() ) { echo 'customtopMargin';}?>">
                    <li class="user-details  darken-2">
                        <div class="row">
                            <div class="col s4 m4 l4 userImage">
								<?php echo get_avatar( get_the_author_email(), '75' ); ?>
                                <!--img src="images/userpic.jpg" alt="" class="circle responsive-img valign profile-image"-->
                            </div>
                            <div class="col col s8 m8 l8">
                                <ul id="profile-dropdown" class="dropdown-content">
                                    <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                                    </li>
                                    <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                                    </li>
                                </ul>
								<p class="user-welcome">Welcome</p>
                                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">
								<?php
									global $current_user;
									if ( is_user_logged_in() ) {
										if ( isset($current_user) ) {
											echo $current_user->user_firstname;
										}
									}else{
										echo 'Guest';
									}
								?><i class="mdi-navigation-arrow-drop-down right"></i>	
								</a>
                                
                            </div>
                        </div>
                    </li>
                    <li class="sidemenu-item active"><a href="/lettpanett/dashboard" class="waves-effect waves-cyan"><i class="fa fa-cubes"></i> Dashboard</a>
                    </li>
                    <li class="sidemenu-item"><a href="/lettpanett/videos" class="waves-effect waves-cyan"><i class="fa fa-video-camera"></i> Videos <span class="new badge">4</span></a>
                    </li>
                    <li class="li-hover"><div class="divider"></div></li>
                    <li class="li-hover"><p class="ultra-small margin more-text">MORE</p></li>
                    <li class="sidemenu-item"><a href="#" class="waves-effect waves-cyan"><i class="fa fa-cog"></i> Settings</a>
                    </li>
                    <li class="sidemenu-item"><a href="#"><i class="fa fa-envelope"></i> Mail <span class="new badge">10</span></a>
					</li>					
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only red1"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
	<div class="bodyWrapper" style="position:relative;">
		<div class="wrapper">
		<div class="content-wrapper  container clearfix">
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="row" style="margin-bottom:0px;">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="post_ col 12">
										<div class="postContent">
											<?php if(!is_front_page()){?>
											<div class="postHeader clearfix">
												<h1 class="post_title"><?php the_title(); ?></h1>
											</div>
											<?php } ?>
											<div class="postText">
												<?php the_content();?>
											</div>
										</div>
									</div>
								
								</div>
			</div>
			<?php endwhile; // End the loop. Whew. ?>
		</div>
		</div>
		
	</div>
</div>
</main>
<?php get_footer('dashboard'); ?>