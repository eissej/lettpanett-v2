<footer class="page-footer topfooter">
	<div class="wrapper">
	<div class="container  clearfix">
		<div class="row">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer area') ) ; ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="copyarea clearfix">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('copy area') ) ; ?>
			</div>
		</div>
	</div>
	</div>
</footer>

<!-- End Document
================================================== -->
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/materialize.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/prism.js"></script>
<!--script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script-->
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js" type="text/javascript"></script>
</body>
</html>