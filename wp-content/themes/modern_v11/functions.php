<?php if(function_exists('register_sidebar')){
	
	register_sidebar(array(
		'name' => 'bottom widget area',
		'before_widget' => '<div id="%1$s" class="withIcons col s12 m3">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widgetTitle_" style="display:none;">',
		'after_title' => '</h2>'
	));
	
	register_sidebar(array(
		'name' => 'footer area',
		'before_widget' => '<div id="%1$s" class="col s12 m3">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgetTitle">',
		'after_title' => '</h3>'
	));
	
	register_sidebar(array(
		'name' => 'sidewidget',
		'before_title' => '<h3 class="sidewidgetTitle">',
		'after_title' => '</h3>',
		'before_widget' => '<div id="%1$s" class="">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => 'banner area',
		'before_widget' => '<div id="%1$s" class="">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgetTitle" style="display:none;">',
		'after_title' => '</h3>'
	));
	
	register_sidebar(array(
		'name' => 'default sidewidget',
		'before_title' => '<h3 class="sidewidgetTitle">',
		'after_title' => '</h3>',
		'before_widget' => '<div id="%1$s" class="defaultwidgetItem">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => 'copy area',
		'before_widget' => '<div id="%1$s" class="col s12 m6">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgetTitle" style="display:none;">',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'name' => 'default bottom widget area',
		'before_widget' => '<div id="%1$s" class="col s12">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgetTitle" style="display:none;">',
		'after_title' => '</h3>'
	));
}
add_theme_support( 'post-thumbnails' );
function custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// NUMERIC PAGINATION

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="custompagination"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}


?>
