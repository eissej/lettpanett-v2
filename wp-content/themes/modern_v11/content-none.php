<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if(!is_front_page()){?>
	<div class="col s12">
		<div class="postHeader clearfix">
			<h1 class="post_title withbottomBorder"><?php _e( 'Oops! Can not find your search.', 'lettpanett' ); ?></h1>
		</div>
	</div>
	<?php } ?>
	<div class="post_ col s12">
		<div class="postContent">
			<div class="postText">
				<p><?php _e( 'Try another search.', 'lettpanett' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</div>