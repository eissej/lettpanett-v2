$(document).ready(function(){
	
	$('a#scrollTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
	
	$('.ginput_container input[type="text"]').each(function(){
  
  var Input = $(this);
    var default_value = Input.val();
    
    $(Input).focus(function() {
        if($(this).val() == default_value)
        {
             $(this).val("");
        }
    }).blur(function(){
        if($(this).val().length == 0) /*Small update*/
        {
            $(this).val(default_value);
        }
    });
	});
	
	
	
	
	$('#mobilemenubutton').click(function(){
		$('.menu-main-menu-container').slideToggle('slow');
		return false;
	});
	
	$('.quick-links,footer .follow-us').removeClass('five').addClass('four');
	//$('.sideWidgets > div').wrapAll('<div class="sidewidgetcontent"></div>');
	
});

