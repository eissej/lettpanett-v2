<?php/*Template Name: Homepage Template*/?>
<?php get_header('home'); ?>
<main>
<div class="bannerWrapper">
	<div class="wrapper">
		<div class="container">
			<div class="row" style="position:relative;margin-bottom:0px;">
				<div class="bannerContent clearfix">
					<div class="col s12 m8">
						<div class="bannerHeadings">
							<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('banner area') ) ; ?>
							<div class="formWrapper">
								<?php gravity_form( 1, false, false, false, '', false ); ?>
							</div>
						</div>
					</div>
					<div class="mascotWrapper">
						<img src="./wp-content/themes/modern_v11/images/mascot.png">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="bodyWrapper" style="position:relative;">
	<div class="wrapper">
	<div class="content-wrapper  container clearfix">
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="row" style="margin-bottom:0px;">
							<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div class="post_ col 12">
									<div class="postContent">
										<?php if(!is_front_page()){?>
										<div class="postHeader clearfix">
											<h1 class="post_title"><?php the_title(); ?></h1>
										</div>
										<?php } ?>
										<div class="postText">
											<?php the_content();?>
										</div>
									</div>
								</div>
							
							</div>
		</div>
		<?php endwhile; // End the loop. Whew. ?>
	</div>
	</div>
	
	<div class="fullwidth bluebg">
		<div class="wrapper">
			<div class="container">
				<div class="row">
					<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('bottom widget area') ) ; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
<?php get_footer(); ?>