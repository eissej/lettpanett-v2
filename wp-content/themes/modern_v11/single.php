<?php get_header(); ?>
<div class="bodyWrapper" style="position:relative;">
	<div class="wrapper">
		<div class="content-wrapper singlepostTemplate  container clearfix">
			<div class="row">
				<div class="col s12 m9">
				
				<?php while ( have_posts() ) : the_post(); ?>
									
									<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										<div class="post_ blogPost">
											<div class="postHeader blogHeader clearfix">
												<div class="col s12 m9" style="padding-left:0px;">
													<div class="blogitemHeading">
														<h2 class="post_title blogitemtitle"><?php the_title(); ?></h2>
														<div class="tagsWrapper">
															by <span class="authorname"><?php echo get_the_author(); ?></span>
															<span class="categorylist">
																<?php
																	$cats = array();
													
																	foreach(get_the_category($post_id) as $c)
																	{
																		$cat = get_category($c);
																		
																		array_push($cats,$cat->name);
																		
																	}

																	if(sizeOf($cats)>0)
																	{
																		
																		$post_categories = implode(',',$cats);
																	} else {
																		$post_categories = "Not Assigned";
																	}

																	echo "in ".$post_categories;
																	
																?>
															</span>
														</div>
													</div>
												</div>
												<div class="date-sp-wrap">
													<div class="blog-date-sp">
														<h3><?php the_time(d); ?></h3><span><?php the_time(M); ?><br><?php the_time(Y); ?></span>
													</div>
													<div class="blog-com-sp"><a href="<?php the_permalink();?>/#comments"><?php comments_number( '0 comments', 'one comment', '% comments' ); ?></a></div>
												</div>
											</div>
											<div class="article_media">
												<?php
													if ( has_post_thumbnail() ) {
														echo '<div class="postthumbImage">';
														the_post_thumbnail();
														echo '</div>';
													} 
												?>
											</div>
											
											
											<div class="postText">
												<?php the_content();?>
											</div>
											
											
										</div>
									</div><!-- #post-## -->
				<div class="commentsWrapper">
					<a name="comments"></a>
					<?php comments_template(); ?>
				</div>
				<?php endwhile; // End the loop. Whew. ?>
				</div>
				<div class="col s12 m3">
					<div class="sidewidgetsWrapper">
						<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidewidget') ) ; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fullwidth bluebg">
		<div class="wrapper">
			<div class="container clearfix">
				<div class="row">
					<div class="col s12">
						<div class="bottomformWrapper">
							<center><h1>Try our customer solution for your business now today!<span>Simply fill in the form below to start...</span></h1></center>
							<?php gravity_form( 1, false, false, false, '', false ); ?>
						</div>
					</div>
					<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('default bottom widget area') ) ; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>